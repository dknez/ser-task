import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { ConfigService } from '@nestjs/config';
import { INestApplication, Logger } from '@nestjs/common';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { UserModule } from './user/user.module';
import { CompanyModule } from './company/company.module';
import { AddressModule } from './address/address.module';
import { EmailerModule } from './emailer/emailer.module';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';

async function bootstrap() {
  const logger = new Logger();

  const app = await NestFactory.create(AppModule);
  const configService = app.get<ConfigService>(ConfigService);
  const port = configService.get('APP_PORT');

  setupApplication(configService, app);
  setupMicroservices(configService, app);
  setupSwagger(app);

  app.startAllMicroservices();

  await app.listen(port);
  logger.log(`Application listening on port ${port}`);
}
bootstrap();

function setupApplication(
  configService: ConfigService,
  app: INestApplication,
): void {
  const apiUrlPrefix = configService.get('API_URL_PREFIX');
  app.setGlobalPrefix(apiUrlPrefix);
}

function setupMicroservices(
  configService: ConfigService,
  app: INestApplication,
): void {
  const mqUser = configService.get('RABBITMQ_USER');
  const mqPassword = configService.get('RABBITMQ_PASSWORD');
  const mqHost = configService.get('RABBITMQ_HOST');
  const mqQueueName = configService.get('RABBITMQ_QUEUE_NAME');

  app.connectMicroservice<MicroserviceOptions>({
    transport: Transport.RMQ,
    options: {
      urls: [`amqp://${mqUser}:${mqPassword}@${mqHost}`],
      queue: mqQueueName,
      queueOptions: {
        durable: true,
      },
    },
  });
}

function setupSwagger(app: INestApplication): void {
  const config = new DocumentBuilder()
    .setTitle('Serapion')
    .setDescription('Serapion API description')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, config, {
    include: [UserModule, CompanyModule, AddressModule, EmailerModule],
  });
  SwaggerModule.setup('swagger', app, document);
}
