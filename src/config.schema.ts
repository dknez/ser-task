import * as Joi from '@hapi/joi';

export const configValidationSchema = Joi.object({
  STAGE: Joi.string().required(),
  APP_PORT: Joi.number().default(3000).required(),
  DB_HOST: Joi.string().default('localhost').required(),
  DB_PORT: Joi.number().default(5432).required(),
  DB_USERNAME: Joi.string().required(),
  DB_PASSWORD: Joi.string().required(),
  DB_DATABASE: Joi.string().required(),
  RABBITMQ_HOST: Joi.string().default('localhost').required(),
  RABBITMQ_PORT: Joi.number().default(5672).required(),
  RABBITMQ_USER: Joi.string().required(),
  RABBITMQ_PASSWORD: Joi.string().required(),
  MT_HOST: Joi.string().required(),
  MT_PORT: Joi.number().default(25).required(),
  MT_USER: Joi.string().required(),
  MT_PASSWORD: Joi.string().required(),
  MT_FROM: Joi.string().default('no-reply@serapion.com').required(),
  MT_TO: Joi.string().default('no-reply@serapion.com').required(),
  MT_SUBJECT: Joi.string().required(),
});
