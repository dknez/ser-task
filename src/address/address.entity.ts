import { Company } from 'src/company/company.entity';
import { User } from 'src/user/user.entity';
import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

@Entity()
export class Address {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @CreateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
  })
  created_at: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    default: () => 'CURRENT_TIMESTAMP(6)',
    onUpdate: 'CURRENT_TIMESTAMP(6)',
  })
  updated_at: Date;

  @Column()
  fullAddress: string;

  @OneToMany(() => User, (user) => user.address, {
    eager: true,
    cascade: ['insert', 'update'],
  })
  users: User[];

  @OneToMany(() => Company, (company) => company.address, {
    eager: true,
    cascade: ['insert', 'update'],
  })
  companies: Company[];
}
