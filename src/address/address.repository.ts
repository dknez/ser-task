import { InternalServerErrorException, Logger } from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { Address } from './address.entity';
import { AddressDto } from './dto/address.dto';

@EntityRepository(Address)
export class AddressRepository extends Repository<Address> {
  private logger = new Logger('AddressRepository');

  async createAddress(addressDto: AddressDto): Promise<Address> {
    this.logger.verbose(
      `Creating a new address: ${JSON.stringify(addressDto)}`,
    );

    const { fullAddress } = addressDto;

    const address = this.create({
      fullAddress,
    });

    return await this.save(address);
  }
}
