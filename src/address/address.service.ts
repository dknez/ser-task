import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserService } from 'src/user/user.service';
import { Address } from './address.entity';
import { AddressRepository } from './address.repository';
import { AddressDto } from './dto/address.dto';
import { UpdateAddressDto } from './dto/update-address.dto';

@Injectable()
export class AddressService {
  private logger = new Logger('AddressService');

  constructor(
    @InjectRepository(AddressRepository)
    private addressRepository: AddressRepository,
  ) {}

  getAddresses(): Promise<Address[]> {
    return this.addressRepository.find();
  }

  createAddress(addressDto: AddressDto): Promise<Address> {
    this.logger.verbose(
      `Creating a new address. Data: ${JSON.stringify(addressDto)}`,
    );

    return this.addressRepository.createAddress(addressDto);
  }

  async getAddressByName(fullAddress: string): Promise<Address> {
    const found = await this.addressRepository.findOne({ fullAddress });

    if (!found) {
      throw new NotFoundException(`Address ${fullAddress} not found.`);
    }
    return found;
  }

  async getAddressById(id: string): Promise<Address> {
    const found = await this.addressRepository.findOne({ id });

    if (!found) {
      throw new NotFoundException(`Address with id: ${id} not found.`);
    }
    return found;
  }

  async deleteAddress(id: string): Promise<void> {
    const result = await this.addressRepository.delete({ id });

    if (result.affected === 0) {
      throw new NotFoundException(`Address with id: ${id} not found.`);
    }
  }

  async updateAddress(
    id: string,
    updateAddressDto: UpdateAddressDto,
  ): Promise<void> {
    const updatedAddress = await this.addressRepository.update(
      id,
      updateAddressDto,
    );

    if (updatedAddress.affected === 0) {
      throw new NotFoundException(`Address with id: ${id} not found.`);
    }
  }
}
