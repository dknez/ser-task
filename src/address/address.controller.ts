import {
  Body,
  Controller,
  Delete,
  Get,
  Logger,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { ApiConsumes, ApiOperation, ApiResponse } from '@nestjs/swagger';
import { Address } from './address.entity';
import { AddressService } from './address.service';
import { AddressDto } from './dto/address.dto';
import { UpdateAddressDto } from './dto/update-address.dto';

@Controller('address')
export class AddressController {
  private logger = new Logger('AddressController');

  constructor(private addressService: AddressService) {}

  @Get()
  @ApiOperation({ summary: 'Retrieve all addresses' })
  @ApiResponse({
    status: 200,
    description: 'Array of found addresses',
    type: [Address],
  })
  getAddresses(): Promise<Address[]> {
    this.logger.verbose(`Retrieving all addresses.`);
    return this.addressService.getAddresses();
  }

  @Get('/:id')
  @ApiOperation({ summary: 'Retrieve single address by its id' })
  @ApiResponse({
    status: 200,
    description: 'Found address entity',
    type: Address,
  })
  getAddressById(@Param('id') id: string): Promise<Address> {
    return this.addressService.getAddressById(id);
  }

  @Post()
  @ApiConsumes('application/json')
  @ApiOperation({ summary: 'Create address' })
  @ApiResponse({
    status: 200,
    description: 'Created address entity.',
    type: Address,
  })
  createAddress(@Body() addressDto: AddressDto): Promise<Address> {
    this.logger.verbose(
      `Creating a new address. Data: ${JSON.stringify(addressDto)}`,
    );
    return this.addressService.createAddress(addressDto);
  }

  @Delete('/:id')
  @ApiOperation({ summary: 'Delete single address by its id' })
  deleteAddress(@Param('id') id: string): Promise<void> {
    this.logger.verbose(`Deleting address with id ${id}`);
    return this.addressService.deleteAddress(id);
  }

  @Patch('/:id')
  @ApiOperation({ summary: 'Update single address by its id' })
  updateAddress(
    @Param('id') id: string,
    @Body() updateAddressDto: UpdateAddressDto,
  ): Promise<void> {
    this.logger.verbose(
      `Updating address with id ${id} with following data: ${JSON.stringify(
        updateAddressDto,
      )}`,
    );
    return this.addressService.updateAddress(id, updateAddressDto);
  }
}
