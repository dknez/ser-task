import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { User } from 'src/user/user.entity';

export class AddressDto {
  @IsNotEmpty()
  @ApiProperty()
  fullAddress: string;
}
