import { Controller, Get, Logger } from '@nestjs/common';
import { EventPattern } from '@nestjs/microservices';
import { ApiOperation, ApiResponse } from '@nestjs/swagger';
import { User } from 'src/user/user.entity';
import { EmailerService } from './emailer.service';

@Controller('emailer')
export class EmailerController {
  constructor(private emailerService: EmailerService) {}

  private logger = new Logger('EmailerController');

  @Get()
  @ApiOperation({ summary: 'Receives "user-created" event.' })
  @ApiResponse({
    status: 200,
  })
  @EventPattern('user-created')
  async handle(data: Record<string, unknown>) {
    this.logger.verbose(
      `Received "user-created" event with following data: ${JSON.stringify(
        data,
      )}. Sending new email.`,
    );

    this.emailerService.sendEmail(data);
  }
}
