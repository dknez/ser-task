import { Injectable, Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import * as nodemailer from 'nodemailer';
import { Transporter } from 'nodemailer';
import SMTPTransport from 'nodemailer/lib/smtp-transport';

@Injectable()
export class EmailerService {
  constructor(private configService: ConfigService) {}

  private logger = new Logger('EmailerService');

  async sendEmail(data: Record<string, unknown>): Promise<void> {
    this.logger.verbose('Sending email.');

    const transporter: Transporter = nodemailer.createTransport({
      host: this.configService.get('MT_HOST'),
      port: this.configService.get('MT_PORT'),
      auth: {
        user: this.configService.get('MT_USER'),
        pass: this.configService.get('MT_PASSWORD'),
      },
    });

    const options = {
      from: this.configService.get('MT_FROM'),
      to: this.configService.get('MT_TO'),
      subject: this.configService.get('MT_SUBJECT'),
      text: JSON.stringify(data),
    };

    try {
      const result: SMTPTransport.SentMessageInfo = await transporter.sendMail(
        options,
      );
      this.logger.verbose(
        `Successfully sent email. More details: ${JSON.stringify(result)}`,
      );
    } catch (error) {
      this.logger.error(`Could not send email. ${error}`);
    }
  }
}
