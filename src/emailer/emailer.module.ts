import { Module } from '@nestjs/common';
import { EmailerController } from './emailer.controller';
import { EmailerService } from './emailer.service';

@Module({
  providers: [EmailerService],
  exports: [EmailerService],
  controllers: [EmailerController],
})
export class EmailerModule {}
