import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional, IsString } from 'class-validator';
import { Address } from 'src/address/address.entity';
import { User } from 'src/user/user.entity';

export class UpdateCompanyDto {
  @IsOptional()
  @IsString()
  @ApiProperty()
  name?: string;

  @IsOptional()
  address: Address;

  // @IsOptional()
  // user: User;
}
