import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { Address } from 'src/address/address.entity';
import { User } from 'src/user/user.entity';

export class CompanyDto {
  @IsNotEmpty()
  @ApiProperty()
  name: string;

  @IsOptional()
  address: Address;
}
