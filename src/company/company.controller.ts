import {
  Body,
  Controller,
  Delete,
  Get,
  Logger,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { ApiConsumes, ApiOperation, ApiResponse } from '@nestjs/swagger';
import { Company } from './company.entity';
import { CompanyService } from './company.service';
import { CompanyDto } from './dto/company.dto';
import { UpdateCompanyDto } from './dto/update-company.dto';

@Controller('company')
export class CompanyController {
  private logger = new Logger('CompanyController');

  constructor(private companyService: CompanyService) {}

  @Get()
  @ApiOperation({ summary: 'Retrieve all companies' })
  @ApiResponse({
    status: 200,
    description: 'Array of found companies',
    type: [Company],
  })
  getCompanies(): Promise<Company[]> {
    this.logger.verbose(`Retrieving all companies.`);
    return this.companyService.getCompanies();
  }

  @Get('/:id')
  @ApiOperation({ summary: 'Retrieve single company by its id' })
  @ApiResponse({
    status: 200,
    description: 'Found company entity',
    type: Company,
  })
  getCompanyById(@Param('id') id: string): Promise<Company> {
    return this.companyService.getCompanyById(id);
  }

  @Post()
  @ApiConsumes('application/json')
  @ApiOperation({ summary: 'Create Company' })
  @ApiResponse({
    status: 200,
    description: 'Created company entity.',
    type: Company,
  })
  createCompany(@Body() companyDto: CompanyDto): Promise<Company> {
    this.logger.verbose(
      `Creating a new company. Data: ${JSON.stringify(companyDto)}`,
    );
    return this.companyService.createCompany(companyDto);
  }

  @Delete('/:id')
  @ApiOperation({ summary: 'Delete single address by its id' })
  deleteCompany(@Param('id') id: string): Promise<void> {
    this.logger.verbose(`Deleting company with id ${id}`);
    return this.companyService.deleteCompany(id);
  }

  @Patch('/:id')
  @ApiOperation({ summary: 'Update single address by its id' })
  updateCompany(
    @Param('id') id: string,
    @Body() updateCompanyDto: UpdateCompanyDto,
  ): Promise<void> {
    this.logger.verbose(
      `Updating company with id ${id} with following data: ${JSON.stringify(
        updateCompanyDto,
      )}`,
    );
    return this.companyService.updateCompany(id, updateCompanyDto);
  }
}
