import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AddressService } from 'src/address/address.service';
import { Company } from './company.entity';
import { CompanyRepository } from './company.repository';
import { CompanyDto } from './dto/company.dto';
import { UpdateCompanyDto } from './dto/update-company.dto';
@Injectable()
export class CompanyService {
  private logger = new Logger('CompanyService');

  constructor(
    @InjectRepository(CompanyRepository)
    private companyRepository: CompanyRepository,

    private addressService: AddressService,
  ) {}

  getCompanies(): Promise<Company[]> {
    return this.companyRepository.find();
  }

  async createCompany(companyDto: CompanyDto): Promise<Company> {
    this.logger.verbose(
      `Creating a new company. Data: ${JSON.stringify(companyDto)}`,
    );

    if (companyDto.address?.fullAddress) {
      await this.createAddressRelation(companyDto);
    }

    return this.companyRepository.createCompany(companyDto);
  }

  async updateCompany(
    id: string,
    updateCompanyDto: UpdateCompanyDto,
  ): Promise<void> {
    const updatedCompany = await this.companyRepository.update(
      id,
      updateCompanyDto,
    );

    if (updatedCompany.affected === 0) {
      throw new NotFoundException(`Company with id: ${id} not found.`);
    }
  }

  async getCompanyById(id: string): Promise<Company> {
    const found = await this.companyRepository.findOne({ id });

    if (!found) {
      return null;
    }
    return found;
  }

  async getCompanyByName(name: string): Promise<Company> {
    const found = await this.companyRepository.findOne({ name });

    if (!found) {
      return null;
    }
    return found;
  }

  async deleteCompany(id: string): Promise<void> {
    const result = await this.companyRepository.delete({ id });

    if (result.affected === 0) {
      throw new NotFoundException(`Company with id: ${id} not found.`);
    }
  }

  private async createAddressRelation(companyDto: CompanyDto) {
    const fullAddress = companyDto.address?.fullAddress;

    this.logger.verbose(`Looking for address ${fullAddress}`);

    const found = await this.addressService.getAddressByName(fullAddress);

    companyDto.address = found
      ? found
      : await this.addressService.createAddress(companyDto.address);
  }
}
