import { Logger } from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { Company } from './company.entity';
import { CompanyDto } from './dto/company.dto';

@EntityRepository(Company)
export class CompanyRepository extends Repository<Company> {
  private logger = new Logger('CompanyRepository');

  async createCompany(companyDto: CompanyDto): Promise<Company> {
    this.logger.verbose(
      `Creating a new company: ${JSON.stringify(companyDto)}`,
    );

    const { name, address } = companyDto;

    const company = this.create({
      name,
      address,
    });

    return await this.save(company);
  }
}
