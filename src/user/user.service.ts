import { Injectable, Logger, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { AddressService } from 'src/address/address.service';
import { CompanyService } from 'src/company/company.service';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserDto } from './dto/user.dto';
import { User } from './user.entity';
import { UserRepository } from './user.repository';

@Injectable()
export class UserService {
  private logger = new Logger('UserService');

  constructor(
    @InjectRepository(UserRepository)
    private userRepository: UserRepository,

    private addressService: AddressService,

    private companyService: CompanyService,
  ) {}

  getUsers(): Promise<User[]> {
    return this.userRepository.find();
  }

  async createUser(userDto: UserDto): Promise<User> {
    this.logger.verbose(
      `Creating a new user. Data: ${JSON.stringify(userDto)}`,
    );

    if (userDto.address?.fullAddress) {
      await this.createAddressRelation(userDto);
    }

    if (userDto.company?.name) {
      await this.createCompanyRelation(userDto);
    }

    return this.userRepository.createUser(userDto);
  }

  async getUserById(id: string): Promise<User> {
    const found = await this.userRepository.findOne({ id });

    if (!found) {
      throw new NotFoundException(`User with id: ${id} not found.`);
    }
    return found;
  }

  async getUserByName(name: string): Promise<User> {
    const found = await this.userRepository.findOne({ name });

    if (!found) {
      throw new NotFoundException(`User with name: ${name} not found.`);
    }

    return found;
  }

  async deleteUser(id: string): Promise<void> {
    const result = await this.userRepository.delete({ id });

    if (result.affected === 0) {
      throw new NotFoundException(`User with id: ${id} not found.`);
    }
  }

  async updateUser(id: string, updateUserDto: UpdateUserDto): Promise<void> {
    const updatedUser = await this.userRepository.update(id, updateUserDto);

    if (updatedUser.affected === 0) {
      throw new NotFoundException(`User with id: ${id} not found.`);
    }
  }

  private async createAddressRelation(userDto: UserDto) {
    const fullAddress = userDto.address?.fullAddress;

    this.logger.verbose(`Looking for address ${fullAddress}`);

    const found = await this.addressService.getAddressByName(fullAddress);

    userDto.address = found
      ? found
      : await this.addressService.createAddress(userDto.address);
  }

  private async createCompanyRelation(userDto: UserDto) {
    const companyName = userDto.company?.name;

    this.logger.verbose(`Looking for address ${companyName}`);

    const found = await this.companyService.getCompanyByName(companyName);

    userDto.company = found
      ? found
      : await this.companyService.createCompany(userDto.company);
  }
}
