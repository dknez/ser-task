import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsOptional } from 'class-validator';
import { Address } from 'src/address/address.entity';
import { Company } from 'src/company/company.entity';

export class UserDto {
  @IsNotEmpty()
  @ApiProperty()
  name: string;

  @IsOptional()
  address: Address;

  @IsOptional()
  company: Company;
}
