import {
  Body,
  Controller,
  Delete,
  Get,
  Inject,
  Logger,
  Param,
  Patch,
  Post,
} from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { ApiConsumes, ApiOperation, ApiResponse } from '@nestjs/swagger';
import { UpdateUserDto } from './dto/update-user.dto';
import { UserDto } from './dto/user.dto';
import { User } from './user.entity';
import { UserService } from './user.service';

@Controller('user')
export class UserController {
  private logger = new Logger('UserController');

  constructor(
    private userService: UserService,
    @Inject('SUBSCRIBERS_SERVICE') private client: ClientProxy,
  ) {}

  async onApplicationBootstrap() {
    try {
      await this.client.connect();
    } catch (error) {
      this.logger.error(error);
    }
  }

  @Get()
  @ApiOperation({ summary: 'Retrieve all users' })
  @ApiResponse({
    status: 200,
    description: 'Array of found users',
    type: [User],
  })
  getUsers(): Promise<User[]> {
    this.logger.verbose(`Retrieving all users.`);

    return this.userService.getUsers();
  }

  @Get('/:id')
  @ApiOperation({ summary: 'Retrieve single user by its id' })
  @ApiResponse({
    status: 200,
    description: 'Found user entity',
    type: User,
  })
  getUserById(@Param('id') id: string): Promise<User> {
    return this.userService.getUserById(id);
  }

  @Post()
  @ApiConsumes('application/json')
  @ApiOperation({ summary: 'Create user' })
  @ApiResponse({
    status: 200,
    description: 'Created user entity.',
    type: User,
  })
  createUser(@Body() userDto: UserDto): Promise<User> {
    this.logger.verbose(
      `Creating a new user. Data: ${JSON.stringify(userDto)}`,
    );
    const result = this.userService.createUser(userDto);

    result
      .then(() => {
        this.publishEvent(userDto);
      })
      .catch((error) => {
        this.logger.error(`Error encountered while creating user: ${error}`);
      });

    return result;
  }

  @Delete('/:id')
  @ApiOperation({ summary: 'Delete single user by its id' })
  deleteUser(@Param('id') id: string): Promise<void> {
    this.logger.verbose(`Deleting user with id ${id}`);
    return this.userService.deleteUser(id);
  }

  @Patch('/:id')
  @ApiOperation({ summary: 'Update single address by its id' })
  updateUser(
    @Param('id') id: string,
    @Body() updateUserDto: UpdateUserDto,
  ): Promise<void> {
    this.logger.verbose(
      `Updating user with id ${id} with following data: ${JSON.stringify(
        updateUserDto,
      )}`,
    );
    return this.userService.updateUser(id, updateUserDto);
  }

  async publishEvent(userDto) {
    this.client.emit('user-created', {
      name: userDto.name,
    });
  }
}
