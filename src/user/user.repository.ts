import { Logger } from '@nestjs/common';
import { EntityRepository, Repository } from 'typeorm';
import { UserDto } from './dto/user.dto';
import { User } from './user.entity';

@EntityRepository(User)
export class UserRepository extends Repository<User> {
  private logger = new Logger('UserRepository');

  async createUser(userDto: UserDto): Promise<User> {
    this.logger.verbose(`Creating a new user: ${JSON.stringify(userDto)}`);

    const { name, address, company } = userDto;

    const user = this.create({
      name,
      address,
      company,
    });

    return await this.save(user);
  }
}
