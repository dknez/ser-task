# Serapion interview assignment

## Requirements to run

- [Docker](https://www.docker.com/products/docker-desktop)

## How to run

1. Position yourself inside `docker` dir
2. Run `docker-compose up` (or `docker-compose up -d` if you don't want to block console)
   - When running for the 1st time, depending on your machine specs, it can take up to couple of minutes because it builds required images (db, mq, backend)
3. When application is up and running, you can use the Postman collection provided in the `demo` dir to test it and send the CRUD requests.

---

## Task list

- 3 entities: `Company`, `User` and `Address` and use `created_at` and `updated_at` timestamps. ✔️
  - ![ER Diagram example](demo/er.jpg)
- Fetch data from all relations when request is sent to the endpoint. ✔️
  - ![Relations example](demo/relations.jpg)
- Provide CRUD endpoints ✔️
  - Check `./demo/serapion.postman_collection.json`
- Publish event in RabbitMQ when new `User` is created. ✔️
  - ![MQ example](demo/mq.jpg)
- Send email when new `User` is created (use Mailtrap) ✔️
  - ![Mailtrap example](demo/mailtrap.jpg)
- Generate API docs by using Swagger. ✔️
  - http://localhost:3000/swagger/

---

### Possible improvements:

- Error handling and logging.
- Not really happy with the way how services healtcheck is being tested (classical Docker "run X before Y" issue).
- TypeORM should have automatically save new entities and their relations but still I had to manually persist child entity in order to link it to the parent entity.
- RabbitMQ is not used in the proper way. Since there was no real use-case scenario, I have just decided to implement a queue where the events will be sent and later on that received event will trigger email sending.

---

### Design:

- For persisting data I've chosen PostgreSQL.
- On backend I am using TypeORM.
- CI/CD
  - No proper CI/CD due to time constraint
  - What can be partially considered as CI/CD is Docker multi stage build
  - Backend Docker multi-stage build does couple of things:
    1. Spins up Postgres instance, creates DB and user
    2. Creates build artifact and packages it as Docker image
- Docker
  - Decided to package db, mq and backend as Ddocker images for easier use during local deployment:
  - db image - PostgreSQL image which contains initial SQL script -> creates serapion db and user
  - mq image - RabbitMQ image which initializes MQ -> creates serapion user and queue
  - Backend image (explained above)
  - Two docker-compose scripts:
    - one located in `./local-dev/`
      - used for local development purposes
      - start DB and MQ but does not start NestJS app automatically
    - another one (mentioned above) located in `./docker/`
      - used for "production"
- testing -> no testing due to time constraint
