# BUILD stage
# 1) install deps and devDeps - required to produce build artifact
# 2) start Postgres and run migrations
# 3) produce build artifact
FROM postgres:13.3-alpine as builder

# postgres envs
ENV POSTGRES_USER postgres
ENV POSTGRES_PASSWORD mysecretpassword
ENV DB_NAME serapion
ENV DB_USER serapion
ENV DB_PASS serapion

COPY . /home/

RUN \
	cd home && \
	apk add --update nodejs npm && \
  npm ci && \
	npm run build

# POST BUILD stage - build artifact only needs deps (not devDeps)
FROM node:14.17-alpine as post_builder

ENV NODE_ENV production
WORKDIR /home/node

COPY package.json package-lock.json /home/node/
RUN npm ci

# RELEASE stage
FROM node:14.17-alpine
ENV NODE_ENV production
WORKDIR /home/node

COPY --from=builder /home/package*.json /home/node/
COPY --from=builder /home/dist/ /home/node/dist/
COPY --from=post_builder /home/node/node_modules/ /home/node/node_modules/

CMD ["node", "dist/main.js"]